<?php
namespace Home\Controller;
use Think\Controller;
class UploadController extends Controller {
    
    public function index(){
    	
    	$this->display();
    }
    public function uploadFile(){
    	$upload = new \Think\Upload();// 实例化上传类
	    $upload->maxSize   =     3145728 ;// 设置附件上传大小
	    $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
	    $upload->rootPath  =     'Uploads/'; // 设置附件上传根目录
	    // 上传文件 
	    $info   =   $upload->upload();
	    if(!$info) {// 上传错误提示错误信息
	        $this->ajaxReturn($upload->getError());
	    }else{// 上传成功
	       // $this->success('上传成功！');
	    	//print_r($info);
	        $msg = array(
			'status' => 1,
			'src' =>"/".$upload->rootPath.$info['filedata']['savepath'].$info['filedata']['savename'],		
			);
			//dump($array);exit;
			$this->ajaxReturn($msg);
	    }
    }
    public function jcrop(){

    	$action=$_GET['action'];
    	//print_r($action);
    	if($action=='jcrop'){
    		$crop = $_POST['crop'];
    		//var_dump($_POST);exit;
			if($crop){
			$targ_w = $targ_h = 100;
			$image = new \Think\Image(); 
			$image->open(".".$crop['path']);
		//将图片裁剪为400x400并保存为corp.jpg
		$image->crop(100, 100,$crop['x'],$crop['y'])->save('./crop.jpg');
			$src = $crop['path']; //图片全地址
			$pathinfo = pathinfo($src);
			// var_dump($pathinfo);exit;
			$filename = '/Uploads/small'.$targ_w.'_'.$pathinfo['basename'];
			// var_dump($filename);exit;
			$img_r = imagecreatefromjpeg($src); //从url新建一图像
			$dst_r = imageCreateTrueColor($targ_w,$targ_h); //创建一个真色彩的图片源
			imagecopyresampled($dst_r,$img_r,0,0,$crop['x'],$crop['y'],$targ_w,$targ_h,$crop['w'],$crop['h']);
			imagejpeg($dst_r,$filename,90);
			
			echo $filename;
			exit;
			}	
    	}
    }
}