<?php 
/**
* @author tian
* @copyright 2015
*/
namespace Admin\Controller;
use Think\Controller;
class BaseController extends Controller
{
	public $_mod;
	protected function _initializes(){}
	protected function _initialize()
	{
		// $this->_mod=D(CONTROLLER_NAME);

		if(!isset($_SESSION['id'])){$this->redirect(C('USER_AUTH_GATEWAY'));}
		if(C('USER_AUTH_ON')&&!in_array(MODULE_NAME, explode(',', C('NOT_AUTH_MODULE')))){
			
			if(!\Org\Util\Rbac::AccessDecision(MODULE_NAME))
			{
				if (!$_SESSION [C('USER_AUTH_KEY')]) {
					//跳转到认证网关
					redirect(PHP_FILE . C('USER_AUTH_GATEWAY'));
				}
				// 没有权限 抛出错误
				if (C('RBAC_ERROR_PAGE')) {
		
					// 定义权限错误页面
					redirect(C('RBAC_ERROR_PAGE'));
				} else {
					if (C('GUEST_AUTH_ON')) {
						$this->assign('jumpUrl', PHP_FILE . C('USER_AUTH_GATEWAY'));
					}
					// 提示错误信息
					$this->error(L('_VALID_ACCESS_'));
		
				}
			}else{
				// print_r($_SESSION);
				return true;
			}

		}else{
			return true;

		}
	}
}

 ?>