<?php 
namespace Admin\Controller;
use Think\Controller;
/**
* @
*/
class LoginController extends Controller
{
	public function index()
	{
		$this->display();
	}
	public function dologin()
	{
		$user=D('User');
		$res=$user->where($_POST)->find();
		if($res)
		{
			session('id',$res['id']);
			session('username',$res['username']);
			if($res['username']==C('RBAC_ADMIN')){
				session(C('ADMIN_AUTH_KEY'),1);
			}
			\Org\Util\Rbac::saveAccessList($res['id']);
			$this->redirect('Role/rolelist');
		}else{
			$this->error('用户名或密码不正确');
		}
	}

	public function logout()
	{
		session('id','');
		session(null);
		session_destroy();
		$this->redirect(U('/Admin/Login'));

	}
}

 ?>