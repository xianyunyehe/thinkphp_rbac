<?php 
namespace Admin\Controller;
use Think\Controller;
use Admin\Controller\BaseController;
/**
* 
*/
class NodeController extends Controller
{
	public function nodelist()
	{
		$node=D('Node');
		$res=$node->select();
		$res=getTree($res);
		$this->assign('nodelist',$res);
		$this->display();
	}
	public function _before_nodeadd()
	{
		$nodename=array(
				'应用',
				'模块',
				'控制器',
				'操作',
			);	
		$this->assign('nodename',$nodename[$_GET['level']]);
	}
	public function nodeadd()
	{
		$level=I('get.level','1','intaval');
		$pid=I('get.pid','0','intval');
		
		if(IS_POST){
			$node=D('Node');
			$res=$node->add($_POST);
			if($res)
				$this->success('添加成功',U('Node/nodelist'));
			else
				$this->error('添加失败');

		}else
		{
			//dump($nodename[$level]);
			//$this->assign('nodename',$nodename[$level]);
			$this->display();
		}
		
	}
	
}


 ?>