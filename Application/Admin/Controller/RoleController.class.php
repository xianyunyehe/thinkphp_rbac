<?php 
namespace Admin\Controller;
use Admin\Controller\BaseController;
/**
* @author
*
*/
class RoleController extends BaseController
{
	public function roleadd()
	{
		if(IS_POST)
			{
				$role=D('Role');
				if(false!==($role->add($_POST)))
				{
					$this->success('添加成功');
				}else{
					$this->error($role->getError());
				}

			}else{
				$this->display();
			}
	}
	public function rolelist($show='1'){
		$role=D('Role');
		$data=$role->select();
		if(!$show) return $data;
		$this->data=$data;
		$this->display();
	}

	public function roledel($id='')
	{
		$id=I('get.id','','intval');
		if(!$id){$this->error('id参数非法');}
		$role=D('Role');
		$res=$role->where(array('id'=>$id))->delete();
		if($res) {$this->success('删除成功',U('rolelist'));}
	}
	public function roleedit(){
		$role=D('Role');
		if(IS_POST)
		{
			$id=I('get.id','','intval');
			if(!$id){$this->error('id参数非法');}
			
			if(false!==($role->save($_POST)))
			{
				$this->success('保存成功');
			}else{
				$this->error($role->getError());
			}
		}else{
			$id=I('get.id','','intval');
			$list=$role->where(array('id'=>$id))->find();
			$this->list=$list;
			$this->display();
		}
		
	}

	public function roleauth(){
		if(IS_POST)
		{
			// dump($_POST);exit;
			$role_id=I('post.role_id','','intval');

			$access=D('Access');
			$access->where(array('role_id'=>$role_id))->delete();
			$data=array();
			foreach($_POST['node_id'] as $k=>$v)
			{
				$data[$k]['role_id']=$role_id;
				$data[$k]['node_id']=$v;
			}
			$res=$access->addAll($data);
			if($res)
			{
				$this->success('授权成功');

			}else{
				$this->error('授权失败');
			}
		}else
		{
			$role_id=I('get.id','','intval');
			$auth=D('Node');
			$access=D('Access');
			$acs=$access->field(array('role_id','node_id'))->where(array('role_id'=>$role_id))->select();
			$authlist=$auth->select();
			$authlist=getTree($authlist);
			$auths=array();
			foreach($authlist as $auth=>&$v)
			{
				foreach($acs as $as=>$v2){
					if($v2['node_id']==$v['id']){
						$v['checked']='1';
					}
					
				}
			}
			$this->assign('authlist',$authlist);
			$this->display();
		}
		
	}
}



 ?>