<?php 
namespace Home\Controller;
use Home\Controller\BaseController;
/**
* @
*/
class AuthGroupController extends BaseController
{
	
	public function groupadd()
	{
		if(IS_POST){
			$group=M('AuthGroup');
			$res=$group->add($_POST);
			if($res)
				$this->success('添加成功',U('AuthGroup/groulist'));
			else
				$this->error('添加失败');

		}else{
			$this->display();
		}

	}
	public function grouplist($show='1'){
		$group=D('AuthGroup');
		$grouplist=$group->select();
		if(!$show) return $groulist;
		$this->assign('grouplist',$grouplist);
		$this->display();

	}
	public function groupedit(){

	}
	public function groupdel(){

	}
	public function grant(){
		$id=I('get.id','','intval');
		if(!$id) $this->error('参数未知');
		$group=D('AuthGroup');
		$rules=$group->field('rules')->where(array('id'=>$id))->find();
		if(strpos(',',$rules['rules'])){
			$rules=explode(',',$rules);
		}else{
			$rules=array($rules['rules']);
		}
		
		$auth=D('AuthRule');
		$auths=$auth->select();
		foreach($auths as $k=>&$v)
		{
			if(in_array($v['id'],$rules)){
				$v['checked']=1;
			}
		}
		$this->assign('authlist',$auths);
		$this->display();
	}
	public function dogrant(){
		if(IS_POST)
		{
			$rules=join(',',$_POST['rules']);
			$id=I('post.id','','intval');
			$group=D('AuthGroup');
			$res=$group->where(array('id'=>$id))->save(array('rules'=>$rules));
			if($res){
				$this->success('授权成功');
			}else{
				$this->error('授权失败');
			}
		}
	}
}

 ?>