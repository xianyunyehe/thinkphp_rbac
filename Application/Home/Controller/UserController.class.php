<?php 
namespace Home\Controller;
use Home\Controller\BaseController;

/**
* @
*/
class UserController extends BaseController
{
	
	

	public function useradd(){
		if(IS_POST)
		{
			$user=D('User');
			//dump($_POST);exit;
				if(false!==($id=$user->add($_POST)))
				{
					$data[]=array();
					foreach($_POST['group_id'] as $key=> $v)
					{
						$data[$key]['uid']=$id;
						$data[$key]['group_id']=$v;
					}
					//dump($data);exit;
					$role_user=D('AuthGroupAccess');
					$res=$role_user->addAll($data);
					if($res) {$this->success('添加成功',U('AuthGroup/grouplist'));}else{$this->error('添加失败');}
				}else{
					$this->error($user->getError());
				}	

		}else{
			$group=D('AuthGroup');
			$grouplist=$group->select();
			$this->assign('roles',$grouplist);
			$this->display();
		}

	}

	public function userlist($show="1")
	{
		$user=D('User');
		$userlist=$user->field(array('id','username','status'))->select();
		if(!$show) return $userlist;
		$this->userlist=$userlist;
		$this->display();
	}

}
 ?>