<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>添加用户</title>
	<link href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<h2>用户列表</h2>
<hr>
  <table class="table table-bordered">
  <tr>
    <th>id</th>
    <th>名称</th>
    <th>状态</th>
    <th>备注</th>
    <th>操作</th>
  </tr>
  <?php if(is_array($userlist)): foreach($userlist as $key=>$vo): ?><tr>
    <td><?php echo ($vo["id"]); ?></td>
    <td><?php echo ($vo["username"]); ?></td>
    <td>
    <?php if($vo['status'] != '0' ): ?>启用
    <?php else: ?> 禁用<?php endif; ?>
</td>
    <td><?php echo ($vo["remark"]); ?></td>
    <td>
    <a href="<?php echo U('User/useredit',array('id'=>$vo['id']));?>">修改</a>

    <a href="<?php echo U('User/userdel',array('id'=>$vo['id']));?>">删除</a>
    </td>
  </tr><?php endforeach; endif; ?>
</table>
</div>
  
</body>
</html>