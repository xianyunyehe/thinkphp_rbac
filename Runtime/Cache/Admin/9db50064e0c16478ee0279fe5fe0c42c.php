<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>添加<?php echo ($nodename); ?></title>
	<link href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<h2>添加<?php echo ($nodename); ?></h2>
<hr>
	<form method="post">
  <div class="form-group">
    <label for="name">填写<?php echo ($nodename); ?>名</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="用户名">
  </div>
  <div class="form-group">
    <label for="title">填写<?php echo ($nodename); ?>标题</label>
    <input type="text" name="title" class="form-control" id="title" placeholder="标题">
  </div>
    <div class="form-group">
    <label for="sort">填写<?php echo ($nodename); ?>排序</label>
    <input type="text" name="sort" class="form-control" id="sort" placeholder="排序">
  </div>
   <div class="form-group">
    <label for="sort">启用<?php echo ($nodename); ?></label>
    <input type="radio" name="status" value="1">启用
    <input type="radio" name="status" value="0">禁用
  </div>
  <input type="hidden" name="pid" value="<?php echo ($_GET['pid']); ?>">
  <input type="hidden" name="level" value="<?php echo ($_GET['level']); ?>">
  <button type="submit" class="btn btn-default">添加<?php echo ($nodename); ?></button>
</form>
</div>
</body>
</html>