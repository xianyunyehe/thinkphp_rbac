<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace Org\Util;
class CutImages{
    
    private $filename;  //原文件全路径
    private $x; //横坐标
    private $y; //纵坐标
    private $x1; //源图宽
    private $y1; //源图高
    private $ext; //文件后缀
    private $width=120; //宽
    private $height=120; //高
    private $jpeg_quality=90; //图片生成的保真度  范围0（质量最差）-100（质量最好）
    
    public function __construct(){
        
    }
    
    /**
     * 初始化截图对象
     * @param unknown_type $filename
     * @param unknown_type $x
     * @param unknown_type $y
     * @param unknown_type $x1
     * @param unknown_type $y1
     */
    public function initialize($filename,$x,$y,$x1,$y1){
        if(file_exists($filename)){
            $this->filename = $filename;
            $pathinfo = pathinfo($filename);
            $this->ext = strtolower($pathinfo['extension']); //将扩展名转为小写
        }else{
            $e = new Exception('the file is not exists!',1050);
            throw $e;
        }
        
        $this->x = $x;
        $this->y = $y;
        $this->x1 = $x1;
        $this->y1 = $y1;
    }
    
    /**
     * 生成截图
     * 根据不同的图片格式生成不同的截图
     */
    public function generateShot(){
        switch($this->ext){
            case 'jpg':
                $this -> generateJpg();
                break;
            case 'png':
                $this -> generatePng();
                break;
            case 'gif':
                $this -> generateGif();
                break;
            default:
                return false;
            
        }
    }
    
    /**
     * 获取生成的小图的文件
     */
    public function getShotName(){
        $pathinfo = pathinfo($this->filename);
        $fileinfo = explode('.',$pathinfo['basename']);
        $cutfilename = $fileinfo[0].'_small'.$this->ext;
        return $pathinfo['dirname'].'/'.$cutfilename;
    }
    
    /**
     * 生成jpg图片
     */
    public function generateJpg(){
        $shotname = $this->getShotName();
        $img_r = imagecreatefromjpeg($this->filename); //从url新建一图像
        $dst_r = imageCreateTrueColor($this->width,$this->height); //创建一个真色源的图片
        imagecopyresampled($dst_r,$img_r,0,0,$this->x,$this->y,$this->width,$this->height,$this->x1,$this->y1);
        imagejpeg($dst_r,$shotname,$this->jpeg_quality);
        
        return $shotname;
    }
    
    /**
     * 生成png图片
     */
    public function generatePng(){
        $shotname = $this->getShotName();
        $img_r = imagecreatefrompng($this->filename); //从url新建一图像
        $dst_r = imageCreateTrueColor($this->width,$this->height); //创建一个真色源的图片
        imagecopyresampled($dst_r,$img_r,0,0,$this->x,$this->y,$this->width,$this->height,$this->x1,$this->y1);
        imagepng($dst_r,$shotname);
        
        return $shotname;
    }
    
    /**
     * 生成gif图片
     */
    public function generateGif(){
        $shotname = $this->getShotName();
        $img_r = imagecreatefromgif($this->filename);
        $dst_r = imageCreateTrueColor($this->width,$this->height);
        imagecopyresampled($dst_r,$img_r,0,0,$this->x,$this->y,$this->width,$this->height,$this->x1,$this->y1);
        if(imagegif($dst_r,$shotname)){
            return $shotname;
        }
        
    }
    
    
}